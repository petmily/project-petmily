import { NavLink } from "react-router-dom";

function Navbar() {
  const activeStyle = {
    textDecoration: "bold",
    color: "white",
    padding: "5px",
    fontWeight: "bold",
    background: " rgb(249, 190, 16)",
    borderRadius: "4px",
  };

  const menuStyle = {
    textDecoration: "none",
    color: "white",

    padding: "5px",
  };

  const ulStyle = {
    display: "flex",
    margin: "30px",

    listStyle: "none",
  };

  return (
    <div>
      <ul style={ulStyle}>
        <li>
          <NavLink
            to="/Prodect"
            style={({ isActive }) => (isActive ? activeStyle : menuStyle)}
          >
            유실유기동물
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/MyPage"
            style={({ isActive }) => (isActive ? activeStyle : menuStyle)}
          >
            마이페이지
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/login"
            style={({ isActive }) => (isActive ? activeStyle : menuStyle)}
          >
            로그인
          </NavLink>
        </li>
      </ul>
    </div>
  );
}

export default Navbar;
