function Header() {
  const headerStyle = {
    padding: "10px",
  };

  return <h4 style={headerStyle}>PETMILY</h4>;
}

export default Header;
